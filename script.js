
function trainer(name, age){
	this.name = name;
	this.age = age;
	this.pokemon = [];
	this.friends = {};
	this.talk = function(target){
		console.log('Pikachu!, I choose you.');
	}
}

let ash = new trainer('Ash Ketchum', 10);
ash.pokemon = ['Pikachu', 'Charizard', "Squirtle", "Balbasaur"]
ash.friends = {
	hoenn: ['Max', "May"],
	kanto: ['Brock', "Misty"]
}
console.log(ash);
console.log('Result of dot notation')
console.log(ash.name);
console.log("Result of Bracket Notation:");
console.log(ash['pokemon']);
console.log('Result of talk method');
ash.talk();

function pokeMon(name, level){
	this.name = name;
	this.level = level;
	this.attack = level;
	this.health = 2 * level;
	this.faint = function(target){
		console.log(`${target.name} has fainted`)
	}
	this.tackle = function(target){
		console.log(`${this.name} tackled ${target.name}`)
		target.health = target.health - this.attack;
		console.log(`${target.name}'s health is now ${target.health}`);
		if (target.health <= 0) {
			this.faint(target);
		}
	}
}

let pikachu = new pokeMon('Pikachu', 12);
let geodude = new pokeMon('Geodude', 8);
let mewtwo = new pokeMon('Mewtwo', 100);
console.log(pikachu);
console.log(geodude);
console.log(mewtwo);

geodude.tackle(pikachu);
console.log(pikachu);
mewtwo.tackle(geodude);
console.log(geodude);


